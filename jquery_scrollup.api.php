<?php

/**
 * Implements hook_jquery_scrollup_admin_prevent_alter().
 *
 * Using this hook, you will be able to override the condition to determine
 * which page is excluded in the context of 'prevent on admin page'.
 *
 * @param $is_admin
 */
function hook_jquery_scrollup_admin_prevent_alter(&$is_admin) {
  // Check custom condition:
  // ...
  $is_admin = TRUE; // or FALSE;
}

/**
 * Implements hook_jquery_scrollup_js_settings_alter().
 *
 * Using this hook, you will be able to override the javascript configuration
 * passed to JS.
 *
 * @param $jsSettings
 */
function hook_jquery_scrollup_js_settings_alter(&$jsSettings) {

  // Override some settings regarding some conditions.
  $someCondition = TRUE;
  if ($someCondition) {
    $jsSettings['button_type'] = 'pill';
  }
}
