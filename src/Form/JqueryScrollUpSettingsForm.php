<?php

namespace Drupal\jquery_scrollup\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Administration settings form.
 */
class JqueryScrollUpSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'jquery_scrollup_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['jquery_scrollup.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#attached']['library'][] = 'jquery_scrollup/jquery_scrollup_admin';

    $config = $this->config('jquery_scrollup.settings');
    $settings = $config->get();

    /* General behavior */
    $form['behavior'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('General behavior')
    ];
    $form['behavior']['preventOnMobile'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Prevent on mobile and touch devices'),
      '#description' => $this->t('Prevents the scrollUp button to appear on touch devices.'),
      '#default_value' => $settings['preventOnMobile'] ?? TRUE,
    ];
    $form['behavior']['preventOnAdminPages'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Prevent on administration pages and node edit'),
      '#description' => $this->t('Prevents the scrollUp button to appear on admin pages.'),
      '#default_value' => $settings['preventOnAdminPages'] ?? TRUE,
    ];
    $form['behavior']['preventOnFrontPage'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Prevent on front page'),
      '#description' => $this->t('Prevent the scrollUp button to appear on front page?'),
      '#default_value' => $settings['preventOnFrontPage'] ?? FALSE,
    ];
    $form['behavior']['scrollDistance'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Trigger distance'),
      '#description' => $this->t('Sets the number of pixels which trigger the scroll button (default 100).'),
      '#default_value' => $settings['scrollDistance'] ?? 100,
      '#size' => 10,
      '#maxlength' => 4,
    ];
    $form['behavior']['scrollSpeed'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scroll speed'),
      '#description' => $this->t('Sets the scroll to top speed in milliseconds (default 300).'),
      '#default_value' => $settings['scrollSpeed'] ?? 300,
      '#size' => 10,
      '#maxlength' => 4,
    ];

    /* Advanced behaviors */
    $form['behavior']['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced behaviors'),
      '#open' => FALSE,
    ];
    $form['behavior']['advanced']['scrollFrom'] = [
      '#type' => 'radios',
      '#title' => $this->t('Trigger distance scroll from'),
      '#description' => $this->t('Determine if the trigger distance configuration should be mesured from top/bottom of the page.'),
      '#options' => [
        'top' => $this->t('Top'),
        'bottom' => $this->t('Bottom'),
      ],
      '#default_value' => $settings['scrollFrom'] ?? 'top',
    ];
    $form['behavior']['advanced']['scrollTarget'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scroll target'),
      '#description' => $this->t('A number (in pixel) or a string (query selector) to scrool to when scrolling to top. Leave empty to scroll to the very top.'),
      '#default_value' => $settings['scrollTarget'] ?? FALSE,
      '#size' => 30,
      '#maxlength' => 30,
    ];

    /* Look and feel */
    $form['ui'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('UI settings: look and feel')
    ];

    $form['ui']['buttonType'] = [
      '#type' => 'radios',
      '#title' => $this->t('The scroll up button display skin'),
      '#description' => $this->t('This module comes up with different basic display skins. If <em>custom</em> is selected, no button will be generated and you will have to add your own one to the page.'),
      '#options' => [
        'icon' => $this->t('SVG icon'),
        'link' => $this->t('Link'),
        'pill' => $this->t('Pill'),
        'tab' => $this->t('Tab'),
        'none' => $this->t('No CSS'),
        'custom' => $this->t('Existing custom element'),
      ],
      '#default_value' => $settings['buttonType'] ?? 'icon',
    ];

    /* Custom case */
    $form['ui']['scrollTrigger'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Scroll trigger'),
      '#description' => $this->t('A jquery selector to the button used to trigger the back to top action.'),
      '#default_value' => $settings['scrollTrigger'] ?? '',
      '#size' => 30,
      '#maxlength' => 30,
      '#states' => [
        'visible' => [
          ':input[name="buttonType"]' => ['value' => 'custom'],
        ],
      ],
    ];

    $form['ui']['buttonText'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button text'),
      '#description' => $this->t('Set the text of the scroll up button'),
      '#default_value' => $settings['buttonText'] ?? $this->t('Back to top'),
      '#states' => [
        'visible' => [
          [':input[name="buttonType"]' => ['value' => 'link']],
          [':input[name="buttonType"]' => ['value' => 'pill']],
          [':input[name="buttonType"]' => ['value' => 'tab']],
          [':input[name="buttonType"]' => ['value' => 'none']],
        ],
      ],
    ];
    $form['ui']['buttonTitle'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button title'),
      '#description' => $this->t('Set the < a > title attribute of the scroll up button is necessary'),
      '#default_value' => $settings['buttonTitle'] ?? '',
      '#states' => [
        'visible' => [
          [':input[name="buttonType"]' => ['value' => 'link']],
          [':input[name="buttonType"]' => ['value' => 'pill']],
          [':input[name="buttonType"]' => ['value' => 'tab']],
          [':input[name="buttonType"]' => ['value' => 'none']],
        ],
      ],
    ];

    $form['ui']['buttonColor'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button icon color'),
      '#description' => $this->t('Color for the button icon. Leave empty for default style.'),
      '#default_value' => $settings['buttonColor'] ?? '',
      '#prefix' => '<div class="color-field-wrapper">',
      '#suffix' => '<div class="color-field" id="edit-buttoncolor-farbtastic"></div></div>',
      '#states' => [
        'visible' => [
          [':input[name="buttonType"]' => ['value' => 'icon']],
          [':input[name="buttonType"]' => ['value' => 'pill']],
          [':input[name="buttonType"]' => ['value' => 'tab']],
        ],
      ],
    ];
    $form['ui']['buttonColorHover'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button icon hover color'),
      '#description' => $this->t('Color for the button icon when hovered. Leave empty for default style.'),
      '#default_value' => $settings['buttonColorHover'] ?? '',
      '#prefix' => '<div class="color-field-wrapper">',
      '#suffix' => '<div class="color-field" id="edit-buttoncolorhover-farbtastic"></div></div>',
      '#states' => [
        'visible' => [
          [':input[name="buttonType"]' => ['value' => 'icon']],
          [':input[name="buttonType"]' => ['value' => 'pill']],
          [':input[name="buttonType"]' => ['value' => 'tab']],
        ],
      ],
    ];
    $form['ui']['buttonBgColor'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button background color'),
      '#description' => $this->t('Color for the button background color. Leave empty for default style.'),
      '#default_value' => $settings['buttonBgColor'] ?? '',
      '#prefix' => '<div class="color-field-wrapper">',
      '#suffix' => '<div class="color-field" id="edit-buttonbgcolor-farbtastic"></div></div>',
      '#states' => [
        'visible' => [
          [':input[name="buttonType"]' => ['value' => 'icon']],
          [':input[name="buttonType"]' => ['value' => 'pill']],
          [':input[name="buttonType"]' => ['value' => 'tab']],
        ],
      ],
    ];
    $form['ui']['buttonBgColorHover'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button background hover color'),
      '#description' => $this->t('Color for the button background color when hovered. Leave empty for default style.'),
      '#default_value' => $settings['buttonBgColorHover'] ?? '',
      '#prefix' => '<div class="color-field-wrapper">',
      '#suffix' => '<div class="color-field" id="edit-buttonbgcolorhover-farbtastic"></div></div>',
      '#states' => [
        'visible' => [
          [':input[name="buttonType"]' => ['value' => 'icon']],
          [':input[name="buttonType"]' => ['value' => 'pill']],
          [':input[name="buttonType"]' => ['value' => 'tab']],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('jquery_scrollup.settings');
    $form_values = $form_state->getValues();

    // scrollTarget conversion.
    $scrollTarget = FALSE;
    if (is_string($form_values['scrollTarget']) && !empty($form_values['scrollTarget'])) {
      $scrollTarget = $form_values['scrollTarget'];
    } elseif (is_int($form_values['scrollTarget'])) {
      $scrollTarget = intval($form_values['preventOnMobile']);
    }

    $config->set('preventOnMobile', $form_values['preventOnMobile'])
      ->set('preventOnAdminPages', $form_values['preventOnAdminPages'])
      ->set('preventOnFrontPage', $form_values['preventOnFrontPage'])
      ->set('scrollDistance', $form_values['scrollDistance'])
      ->set('scrollFrom', $form_values['scrollFrom'])
      ->set('scrollSpeed', $form_values['scrollSpeed'])
      ->set('scrollTarget', $scrollTarget)
      // ->set('buttonPlace', $form_values['buttonPlace'])
      ->set('buttonType', $form_values['buttonType'])
      ->set('buttonText', $form_values['buttonText'])
      ->set('buttonTitle', $form_values['buttonTitle'])

      ->set('buttonColor', $form_values['buttonColor'])
      ->set('buttonColorHover', $form_values['buttonColorHover'])
      ->set('buttonBgColor', $form_values['buttonBgColor'])
      ->set('buttonBgColorHover', $form_values['buttonBgColorHover'])

      ->save();
    parent::submitForm($form, $form_state);
  }
}
