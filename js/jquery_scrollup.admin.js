(function ($) {
    Drupal.behaviors.jquery_scrollup_admin = {
        attach: function(context) {
            $(document).ready(function() {
                $("#edit-buttoncolor-farbtastic").farbtastic("#edit-buttoncolor");
                $("#edit-buttoncolorhover-farbtastic").farbtastic("#edit-buttoncolorhover");
                $("#edit-buttonbgcolor-farbtastic").farbtastic("#edit-buttonbgcolor");
                $("#edit-buttonbgcolorhover-farbtastic").farbtastic("#edit-buttonbgcolorhover");
            });
        }
    };
})(jQuery);
