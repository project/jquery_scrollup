/**
 * @file
 * Run the scrollUp configuration.
 */

(function ($, Drupal) {

  Drupal.behaviors.jquery_scrollup = {
    attach: function (context, settings) {
      var config = settings.jquery_scrollup;
      $('body').once('jquery_scrollup').scrollUp({
        scrollName: 'scrollUp',                       // Element ID
        scrollDistance: config.scrollDistance,        // Distance from top/bottom before showing element (px)
        scrollFrom: config.scrollFrom,                // 'top' or 'bottom'
        scrollSpeed: config.scrollSpeed,              // Speed back to top (ms)
        easingType: 'linear',        // Scroll to top easing (see http://easings.net/)
        animation: 'fade',           // Fade, slide, none
        animationSpeed: 200,         // Animation speed (ms)
        scrollTrigger: false,        // Set a custom triggering element. Can be an HTML string or jQuery object
        scrollTarget: config.scrollTarget,            // Set a custom target element for scrolling to. Can be element or number
        scrollText: config.buttonText,                // Text for element, can contain HTML
        scrollTitle: config.buttonTitle,              // Set a custom <a> title if required.
        scrollImg: (config.buttonType === 'icon'),    // Set true to use image
        activeOverlay: false,        // Set CSS color to display scrollUp active point, e.g '#00FFFF'
        zIndex: 2147483647           // Z-Index for the overlay
      });
      $('#scrollUp').addClass('jquery_scrollup--' + config.buttonType);
    }
  }
})(jQuery, Drupal);